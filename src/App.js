import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { uploadFile, deleteFile } from 'react-s3';

const config = {
	  mode: 'no-cors',
    bucketName: 'learnit-2018-mattitest',
    region: 'eu-west-1',
    accessKeyId: 'AKIAJ72NL2T7ARHWPCFA',
    secretAccessKey: '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',
}

class App extends Component {
	constructor() {
		super();
		this.upload = this.upload.bind(this);
		this.delete = this.delete.bind(this);
		this.state = {
			figureURL: '',
			figureName: ''
		}
	}
	
	upload(e) {
		console.log(e.target.files[0]);
		uploadFile( e.target.files[0], config)
		.then( (data) => {
			console.log("Tiedot:", data);
			console.log("Kuva löytyy:", data.location);
			this.setState({ figureURL: data.result.url});
			this.setState({ figureName: data.key});
		})
		.catch( (err) => {
			alert(err);
		})
	}
	
	delete(e) {
		console.log("Tiedosto on nimeltään :", this.state.figureName);
		deleteFile( this.state.figureName , config)
		.then( (data) => {
			console.log("Tiedot:", data);
			this.setState({ figureURL: '' });
			this.setState({ figureName: '' });
		})
		.catch((err) => {
			alert(err);
		})
	}
	
  render() {
		const loadFigure = this.state.figureURL+this.state.figureName;
		console.log("Load figure:", loadFigure);
    return (
      <div className="App">
				<h1>React AWS S3 upload example</h1>
				<div className="Photo">
					<img src = {loadFigure} /><br />
					<input type="file" onChange={this.upload} /><br /><br />
					<div className="Remove">
						<button onClick={this.delete}>Poista tiedosto</button>
					</div>
				</div>
      </div>
    );
  }
}

export default App;
